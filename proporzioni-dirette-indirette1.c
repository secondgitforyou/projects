#include <stdio.h>
#include <math.h>

int main(void) {
  // variabili per le quantità dirette e indirette
  int a, b, c, d;

  // chiedi all'utente di inserire i valori delle quantità dirette
  printf("Inserisci il valore della quantità diretta A: ");
  scanf("%d", &a);
  printf("Inserisci il valore della quantità diretta B: ");
  scanf("%d", &b);

  // calcola la proporzione diretta utilizzando la funzione isclose()
  // per verificare che la proporzione sia esatta
  double proporzione_diretta = (double)a / b;
  if (isclose(proporzione_diretta, (double)a / b)) {
    printf("La proporzione diretta è %.2f\n", proporzione_diretta);
  } else {
    printf("La proporzione diretta non è esatta.\n");
  }

  // chiedi all'utente di inserire i valori delle quantità indirette
  printf("Inserisci il valore della quantità indiretta C: ");
  scanf("%d", &c);
  printf("Inserisci il valore della quantità indiretta D: ");
  scanf("%d", &d);

  // calcola la proporzione indiretta utilizzando la funzione isclose()
  // per verificare che la proporzione sia esatta
  double proporzione_indiretta = (double)d / c;
  if (isclose(proporzione_indiretta, (double)a / b)) {
    printf("La proporzione indiretta è %.2f\n", proporzione_indiretta);
  } else {
    printf("La proporzione indiretta non è esatta.\n");
  }

  return 0;
}


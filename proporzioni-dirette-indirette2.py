# chiedi all'utente i dati per calcolare le proporzioni
print("Inserisci il valore della quantità nota:")
qn = float(input())
print("Inserisci il valore della quantità sconosciuta:")
qs = float(input())
print("Inserisci il valore del rapporto noto:")
rn = float(input())

# calcola la proporzione diretta
pd = qn * rn
print("La proporzione diretta è:", pd)

# calcola la proporzione indiretta
pi = qs * rn
print("La proporzione indiretta è:", pi)

import math

# chiedi all'utente di inserire i valori delle quantità dirette
a = int(input("Inserisci il valore della quantità diretta A: "))
b = int(input("Inserisci il valore della quantità diretta B: "))

# calcola la proporzione diretta utilizzando la funzione math.isclose()
# per verificare che la proporzione sia esatta
proporzione_diretta = a / b
if math.isclose(proporzione_diretta, a / b):
  print(f"La proporzione diretta è {proporzione_diretta:.2f}")
else:
  print("La proporzione diretta non è esatta.")

# chiedi all'utente di inserire i valori delle quantità indirette
c = int(input("Inserisci il valore della quantità indiretta C: "))
d = int(input("Inserisci il valore della quantità indiretta D: "))

# calcola la proporzione indiretta utilizzando la funzione math.isclose()
# per verificare che la proporzione sia esatta
proporzione_indiretta = d / c
if math.isclose(proporzione_indiretta, a / b):
  print(f"La proporzione indiretta è {proporzione_indiretta:.2f}")
else:
  print("La proporzione indiretta non è esatta.")

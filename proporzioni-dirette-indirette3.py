# Chiediamo all'utente se si tratta di una proporzione diretta o indiretta
tipo_proporzione = input("Si tratta di una proporzione diretta o indiretta? ")

# In base alla risposta dell'utente, chiediamo il valore della proporzione
if tipo_proporzione.lower() == "diretta":
    proporzione = float(input("Inserisci il valore della proporzione diretta: "))
    proporzione_opposta = float(input("Inserisci il valore della proporzione indiretta: "))
else:
    proporzione = float(input("Inserisci il valore della proporzione indiretta: "))
    proporzione_opposta = float(input("Inserisci il valore della proporzione diretta: "))

# Calcoliamo la proporzione totale come somma della proporzione diretta e indiretta
proporzione_totale = proporzione + proporzione_opposta

# Calcoliamo la percentuale della proporzione diretta
percentuale_diretta = (proporzione / proporzione_totale) * 100

# Calcoliamo la percentuale della proporzione indiretta
percentuale_indiretta = (proporzione_opposta / proporzione_totale) * 100

# Stampiamo i risultati
print("La proporzione diretta è del " + str(percentuale_diretta) + "%")
print("La proporzione indiretta è del " + str(percentuale_indiretta) + "%")
